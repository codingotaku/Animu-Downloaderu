# Animu-Downloaderu

## Notes

1. Now has three Servers (Anime1, AnimeFreak, and GogoAnime1) all are in essence the same site (same content hosted differently).
1. Uses [AnimeCrawlerAPI](https://codeberg.org/codingotaku/AnimeCrawlerAPI) for fetching Anime.
1. Modified code so that it would be easier to add new servers in the future! ([this file](./src/main/java/com/codingotaku/apps/source/AnimeSources.java))
We might add more servers in the future! 
1. Android version is not the top priority as of now.

 **I do not support piracy!**, The only reason for me making this app was because of the DRM (Digital Restrictions Management) and licensing that mainstream anime hosing services has and the ad+tracking malware other websites adopted to.
___

## About the App

Animu Downloaderu is a java based software which helps you download your favorite anime as a batch. 

**To build from Source**
Run

```bash
 mvn javafx:run
```

**To get the native app image**
Run

```bash
  mvn javafx:jlink
```

## Screenshots

**Huge list of anime to choose from**
![screenshot](https://codeberg.org/codingotaku/Animu-Downloaderu/raw/branch/master/screenshots/anime-list.webp "anime list screenshot")

**Search your favorite anime**
![Search anime](https://codeberg.org/codingotaku/Animu-Downloaderu/raw/branch/master/screenshots/search.webp "Search anime")

**Download all episodes in a batch**
![Download](https://codeberg.org/codingotaku/Animu-Downloaderu/raw/branch/master/screenshots/download.webp "Download")

### This is not under development anymore and the code is a bit spaghettified so feel free to fork :)

